﻿// See https://aka.ms/new-console-template for more information

using DiamondKata;

var diamondGenerator =
    new DiamondGenerator(new EnglishUpperCaseCharacterProvider(), new SimpleDiamondFormatter(), new WhiteSpacePaddingProvider());
var diamond = diamondGenerator.Generate(Convert.ToChar(args[0]));
Console.WriteLine(diamond);