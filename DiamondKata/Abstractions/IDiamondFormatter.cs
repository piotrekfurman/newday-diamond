﻿namespace DiamondKata.Abstractions;

public interface IDiamondFormatter
{
    string Format(IEnumerable<string> lines);
}