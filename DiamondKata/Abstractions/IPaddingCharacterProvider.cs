﻿namespace DiamondKata.Abstractions;

public interface IPaddingCharacterProvider
{
    char GetPaddingCharacter();
}