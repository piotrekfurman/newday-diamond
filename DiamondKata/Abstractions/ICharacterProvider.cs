﻿namespace DiamondKata.Abstractions;

public interface ICharacterProvider
{
    int GetIndexOf(char c);
    char GetCharacterAt(int index);
}