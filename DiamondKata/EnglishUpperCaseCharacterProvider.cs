﻿using DiamondKata.Abstractions;

namespace DiamondKata;

public class EnglishUpperCaseCharacterProvider : ICharacterProvider
{
    private const int AlphabetSize = 26;
    private static readonly List<char> Alphabet = Enumerable.Range('A', AlphabetSize).Select(i => (char)i).ToList();

    public int GetIndexOf(char c) => Alphabet.IndexOf(c);
    public char GetCharacterAt(int index) => Alphabet[index];
}