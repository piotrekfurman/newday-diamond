﻿using DiamondKata.Abstractions;

namespace DiamondKata;

public class DiamondGenerator
{
    private readonly ICharacterProvider _characterProvider;
    private readonly IDiamondFormatter _formatter;
    private readonly char _paddingCharacter;
    private readonly char _topAndBottomCharacter;

    public DiamondGenerator(
        ICharacterProvider characterProvider, 
        IDiamondFormatter formatter,
        IPaddingCharacterProvider paddingCharacterProvider)
    {
        _characterProvider = characterProvider;
        _formatter = formatter;
        _paddingCharacter = paddingCharacterProvider.GetPaddingCharacter();
        _topAndBottomCharacter = _characterProvider.GetCharacterAt(0);
    }

    public string Generate(char c)
    {
        var indexOfDiamondCenter = _characterProvider.GetIndexOf(c);
        if (indexOfDiamondCenter == 0)
        {
            return _characterProvider.GetCharacterAt(indexOfDiamondCenter).ToString();
        }

        var diamondLines = new List<string>();
        for (var currentIndex = 0; currentIndex <= indexOfDiamondCenter; currentIndex++)
        {
            diamondLines.Add(GenerateLine(currentIndex, indexOfDiamondCenter));
        }

        for (var currentIndex = indexOfDiamondCenter - 1; currentIndex >= 0; currentIndex--)
        {
            diamondLines.Add(GenerateLine(currentIndex, indexOfDiamondCenter));
        }

        return _formatter.Format(diamondLines);
    }

    private string GenerateLine(int currentIndex, int indexOfDiamondCenter)
    {
        var paddingSide = indexOfDiamondCenter - currentIndex;
        var paddingMiddle = currentIndex == 0 ? 0 : 2 * currentIndex - 1;
        var character = _characterProvider.GetCharacterAt(currentIndex);
        var line = string.Empty.PadLeft(paddingSide, _paddingCharacter) + character;
        if (character != _topAndBottomCharacter)
        {
            line += string.Empty.PadLeft(paddingMiddle, _paddingCharacter) + character;
        }

        line += string.Empty.PadLeft(paddingSide, _paddingCharacter);
        return line;
    }
}
