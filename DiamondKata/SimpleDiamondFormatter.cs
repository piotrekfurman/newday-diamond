﻿using DiamondKata.Abstractions;

namespace DiamondKata;

public class SimpleDiamondFormatter : IDiamondFormatter
{
    public string Format(IEnumerable<string> lines) => string.Join("\n", lines);
}
