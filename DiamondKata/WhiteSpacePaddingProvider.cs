﻿using DiamondKata.Abstractions;

namespace DiamondKata;

public class WhiteSpacePaddingProvider : IPaddingCharacterProvider
{
    private const char PaddingChar = ' ';
    public char GetPaddingCharacter() => PaddingChar;
}