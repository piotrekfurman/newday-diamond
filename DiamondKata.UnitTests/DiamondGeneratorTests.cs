using FluentAssertions;

namespace DiamondKata.UnitTests;

public class DiamondGeneratorTests
{
    private readonly DiamondGenerator _generator = new DiamondGenerator(
        new EnglishUpperCaseCharacterProvider(),
        new SimpleDiamondFormatter(), 
        new WhiteSpacePaddingProvider());

    [Fact]
    public void Generate_WithCharA_ReturnsSingleA()
    {
        var expected = "A";
        var result = _generator.Generate('A');
        result.Should().Be(expected);
    }

    [Fact]
    public void Generate_WithCharC_CreatesDiamond()
    {
        var expected = "  A  \n" +
                       " B B \n" +
                       "C   C\n" +
                       " B B \n" +
                       "  A  ";
        var result = _generator.Generate('C');
        result.Should().Be(expected);
    }
    
    [Theory]
    [InlineData('B')]
    [InlineData('Y')]
    [InlineData('T')]
    [InlineData('O')]
    [InlineData('Z')]
    [InlineData('Q')]
    public void Generate_WithLetter_IsSymmetrical(char letter)
    {
        var result = _generator.Generate(letter);
        var lines = result.Split('\n');

        foreach (var line in lines)
        {
            line.Should().BeEquivalentTo(string.Join("", line.Reverse()));
        }

        for (int i = 0; i < lines.Length / 2; i++)
        {
            lines[i].Should().Be(lines[lines.Length - 1 - i]);
        }
    }

    [Fact]
    public void Generate_WithInvalidChar_ReturnsEmptyString()
    {
        var result = _generator.Generate('1');
        result.Should().BeEmpty();
    }

    [Fact]
    public void Generate_WithEmptyChar_ReturnsEmptyString()
    {
        var result = _generator.Generate('\0');
        result.Should().BeEmpty();
    }
}

